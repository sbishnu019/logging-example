import logging.config

from slack import Slack


class SlackHandler(logging.Handler):
    def emit(self, record):
        # print(record.exc_text)
        Slack(
            text='Test',
            blocks_context={
                'header': f"TEST",
                'sections': {
                    'EXCEPTION': record.exc_text
                }
            }
        ).send(hook_url='https://hooks.slack.com/services/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')


class MainHandler(logging.Handler):
    def emit(self, record):
        # print(record.exc_text)
        Slack(
            text='Test',
            blocks_context={
                'header': f"TEST",
                'sections': {
                    'EXCEPTION': record.exc_text
                }
            }
        ).send(hook_url='')


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
                      "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "slack": {
            "level": "ERROR",
            "class": "handler.SlackHandler",
            "formatter": "verbose",
        },
    },
    "root": {"level": "INFO", "handlers": ["console"]},
    "loggers": {
        "simple": {
            "handlers": ["console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "slackLog": {
            "handlers": ["console", "slack"],
            "level": "ERROR",
            "propagate": True,
        }
    },
}

# log configuration
logging.config.dictConfig(LOGGING)

# implementation
logger = logging.getLogger('slackLog')

# logger.debug('This is a debug message')
# logger.info('This is an info message')
# logger.warning('This is a warning message')
# logger.error('This is an error message')
# logger.critical('This is a critical message')


try:
    print(6 / 0)
except Exception as e:
    logger.error('exception occurred', exc_info=True)
