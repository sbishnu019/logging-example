import logging

logging.basicConfig(
    filename='app.log',
    filemode='w',
    # format='%(name)s - %(levelname)s - %(message)s'
    format="%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s"
)
logging.warning('This will get logged to a file')
logging.error('This will get logged to a file')
